package nl.bitart.cubes3d;


import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


public class Cube {

    private final int          x;
    private final int          y;
    private final int          z;
    private final int          r;
    private final String       name;
    private final Set<Face>    faces;

    public Cube(int x, int y, int z, int r, String name) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.r = r;
        this.name = name;
        this.faces = extractOuterFaces();
    }

    private Set<Face> extractOuterFaces() {
        Set<Face> faces = new HashSet<>();

        for (int k = 0; k < r; k++) {
            for (int m = 0; m < r; m++) {
                faces.add(new Face(x, y + k, z + m, Orientation.RIGHT));
                faces.add(new Face(x + r, y + k, z + m, Orientation.RIGHT));

                faces.add(new Face(x + k, y, z + m, Orientation.TOP));
                faces.add(new Face(x + k, y + r, z + m, Orientation.TOP));

                faces.add(new Face(x + k, y + m, z, Orientation.FRONT));
                faces.add(new Face(x + k, y + m, z + r, Orientation.FRONT));
            }
        }

        return faces;
    }

    Set<Cube> extractCubiclesFromCube() {
        Set<Cube> cubicles = new HashSet<>();

        for (int i = x; i < r + x; i++) {
            for (int j = y; j < r + y; j++) {
                for (int k = z; k < r + z; k++) {
                    Cube c = new Cube(i, j, k,1, "cubicle_" + i + "_" + j + "_" + k);
                    cubicles.add(c);
                }
            }
        }
        return cubicles;
    }

    public Set<Face> getOuterFaces() {
        return faces;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Cube cube = (Cube) o;
        return x == cube.x &&
                y == cube.y &&
                z == cube.z &&
                r == cube.r;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z, r);
    }

    @Override
    public String toString() {
        return name;
    }
}
