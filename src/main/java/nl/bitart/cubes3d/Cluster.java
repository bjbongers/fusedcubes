package nl.bitart.cubes3d;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class Cluster {

    private final List<Cube> cubes;

    private final Set<Cube> cubicles;

    private final Set<Face> faces;

    public Cluster() {
        cubes = new ArrayList<>();
        cubicles = new HashSet<>();
        faces = new HashSet<>();
    }

    public void addCube(Cube cube) {
        cubes.add(cube);
        cubicles.addAll(cube.extractCubiclesFromCube());
        faces.addAll(cube.getOuterFaces());
    }

    public List<Cube> getCubes() {
        return cubes;
    }

    public boolean cubicleBelongsToCluster(Cube cubicle) {
        return cubicle.getOuterFaces().stream().anyMatch(faces::contains);
    }

    @Override
    public String toString() {
        return cubes + " --> size = " + cubicles.size();
    }

}
