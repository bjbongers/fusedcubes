package nl.bitart.cubes3d;


import java.util.Objects;


public class Face {

    private final int         x;
    private final int         y;
    private final int         z;
    private final Orientation orientation;


    public Face(int x, int y, int z, Orientation orientation) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.orientation = orientation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Face face = (Face) o;
        return x == face.x &&
                y == face.y &&
                z == face.z &&
                orientation == face.orientation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z, orientation);
    }

    @Override
    public String toString() {
        return "Face{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", orientation=" + orientation +
                '}';
    }
}