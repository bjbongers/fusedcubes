package nl.bitart.cubes3d;


import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


class Space {

    public String calculateClusters(List<Cube> cubes) {

        Set<Cluster> currentClusters = new LinkedHashSet<>();

        for (Cube cube : cubes) {
            List<Cluster> overlappingClusters = getOverlappingClusters(currentClusters, cube);

            if (overlappingClusters.size() == 0) {
                Cluster cluster = new Cluster();
                currentClusters.add(cluster);
                cluster.addCube(cube);
            }
            if (overlappingClusters.size() == 1) {
                Cluster cluster = overlappingClusters.get(0);
                cluster.addCube(cube);
            }
            if (overlappingClusters.size() > 1) {
                Cluster cluster = mergeClusters(currentClusters, overlappingClusters);
                currentClusters.add(cluster);
                cluster.addCube(cube);
            }
        }

        return convertClustersToString(currentClusters);
    }

    private Cluster mergeClusters(Set<Cluster> currentClusters, List<Cluster> overlappingClusters) {
        Cluster merged = new Cluster();

        for (Cluster c : overlappingClusters) {
            for (Cube s : c.getCubes()) {
                merged.addCube(s);
            }
            currentClusters.remove(c);
        }
        return merged;
    }

    private List<Cluster> getOverlappingClusters(Set<Cluster> currentClusters, Cube cube) {
        return currentClusters.stream().filter(cluster -> hasOverlap(cluster, cube)).collect(Collectors.toList());
    }

    private boolean hasOverlap(Cluster cluster, Cube cube) {
        Set<Cube> cubicles = cube.extractCubiclesFromCube();
        return cubicles.stream().anyMatch(cluster::cubicleBelongsToCluster);
    }

    private String convertClustersToString(Set<Cluster> clusters) {
        return clusters.stream().map(c -> c.toString() + "\n").collect(Collectors.joining());
    }
}
