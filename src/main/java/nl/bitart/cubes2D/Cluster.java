package nl.bitart.cubes2D;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


class Cluster {

    private final List<Square> squares;

    private final Set<Square> cublicles;

    private final Set<Edge> edges;

    public Cluster() {
        squares = new ArrayList<>();
        cublicles = new HashSet<>();
        edges = new HashSet<>();
    }

    public void addSquare(Square square) {
        squares.add(square);
        cublicles.addAll(square.extractCubiclesFromSquare());
        edges.addAll(square.getOuterEdges());
    }

    public List<Square> getSquares() {
        return squares;
    }

    @Override
    public String toString() {
        return squares + " --> size = " + cublicles.size();
    }

    public boolean cubicleBelongsToCluster(Square cubicle) {
        return cubicle.getOuterEdges().stream().anyMatch(edges::contains);
    }
}
