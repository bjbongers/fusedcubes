package nl.bitart.cubes2D;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


class Space {

    public String calculateClusters(List<Square> squares) {

        Set<Cluster> currentClusters = new LinkedHashSet<>();

        for (Square square : squares) {
            List<Cluster> overlappingClusters = getOverlappingClusters(currentClusters, square);

            if (overlappingClusters.size() == 0) {
                Cluster cluster = new Cluster();
                currentClusters.add(cluster);
                cluster.addSquare(square);
            }
            if (overlappingClusters.size() == 1) {
                Cluster cluster = overlappingClusters.get(0);
                cluster.addSquare(square);
            }
            if (overlappingClusters.size() > 1) {
                Cluster cluster = mergeClusters(currentClusters, overlappingClusters);
                currentClusters.add(cluster);
                cluster.addSquare(square);
            }
        }

        return convertClustersToString(currentClusters);
    }

    private Cluster mergeClusters(Set<Cluster> currentClusters, List<Cluster> overlappingClusters) {
        Cluster merged = new Cluster();

        for (Cluster c : overlappingClusters) {
            for (Square s : c.getSquares()) {
                merged.addSquare(s);
            }
            currentClusters.remove(c);
        }
        return merged;
    }

    private List<Cluster> getOverlappingClusters(Set<Cluster> currentClusters, Square square) {
        return currentClusters.stream().filter(cluster -> hasOverlap(cluster, square)).collect(Collectors.toList());
    }

    private boolean hasOverlap(Cluster cluster, Square square) {
        Set<Square> cubicles = square.extractCubiclesFromSquare();
        return cubicles.stream().anyMatch(cluster::cubicleBelongsToCluster);
    }

    private String convertClustersToString(Set<Cluster> clusters) {
        return clusters.stream().map(c -> c.toString() + "\n").collect(Collectors.joining());
    }
}
