package nl.bitart.cubes2D;


import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


class Square {

    private final int       x;
    private final int       y;
    private final int       r;
    private final String    name;
    private final Set<Edge> edges;

    public Square(int x, int y, int r, String name) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.name = name;
        this.edges = extractOuterEdges();
    }

    private Set<Edge> extractOuterEdges() {
        Set<Edge> edges = new HashSet<>();

        // onder en boven edges
        for (int k = 0; k < r; k++) {
            edges.add(new Edge(x + k, y, Orientation.HORIZONAL));
            edges.add(new Edge(x + k, y + r, Orientation.HORIZONAL));
        }

        // linker + rechter edges
        for (int k = 0; k < r; k++) {
            edges.add(new Edge(x, y + k, Orientation.VERTICAL));
            edges.add(new Edge(x + r, y + k, Orientation.VERTICAL));
        }

        return edges;
    }

    public Set<Edge> getOuterEdges() {
        return edges;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Square square = (Square) o;
        return x == square.x &&
                y == square.y &&
                r == square.r;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, r);
    }

    public Set<Square> extractCubiclesFromSquare() {
        Set<Square> cubicles = new HashSet<>();

        for (int i = x; i < r + x; i++) {
            for (int j = y; j < r + y; j++) {
                Square c = new Square(i, j, 1, "cublicle_" + i + "_" + j);
                cubicles.add(c);
            }
        }
        return cubicles;
    }
}
