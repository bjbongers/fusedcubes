package nl.bitart.cubes3d;


import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class Cubes3DTest {

    private final Space fixture = new Space();


    @Test
    void two_adjecent_unit_kubes() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(0, 0, 0, 1, "S1"));
        cubes.add(new Cube(1, 0, 0, 1, "S2"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1, S2] --> size = 2\n");
    }

    @Test
    void two_adjecent_unit_kubes_negativex() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(0, 0, 0, 1, "S1"));
        cubes.add(new Cube(-1, 0, 0, 1, "S2"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1, S2] --> size = 2\n");
    }

    @Test
    void two_adjecent_unit_kubes_negative_y() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(0, 0, 0, 1, "S1"));
        cubes.add(new Cube(0, -1, 0, 1, "S2"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1, S2] --> size = 2\n");
    }

    @Test
    void two_adjecent_unit_kubes_negative_z() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(0, 0, 0, 1, "S1"));
        cubes.add(new Cube(0, 0, -1, 1, "S2"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1, S2] --> size = 2\n");
    }

    @Test
    void fused() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(0, 0, 0, 3, "S1"));
        cubes.add(new Cube(1, 2, 2, 3, "S2"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1, S2] --> size = 52\n");
    }

    @Test
    void touch_with_faces() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(0, 0, 0, 3, "S1"));
        cubes.add(new Cube(1, 3, 2, 3, "S2"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1, S2] --> size = 54\n");
    }

    //sorted(fused_cubes([(0, 0, 0, 3), (1, 3, 3, 3)])) == [27, 27] # touch with edges

    @Test
    void touch_with_edges() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(0, 0, 0, 3, "S1"));
        cubes.add(new Cube(1, 3, 3, 3, "S2"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1] --> size = 27\n" + "[S2] --> size = 27\n");
    }

    @Test
    void touch_with_vertices() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(0, 0, 0, 3, "S1"));
        cubes.add(new Cube(3, 3, 3, 3, "S2"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1] --> size = 27\n" + "[S2] --> size = 27\n");
    }

    @Test
    void seperated() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(0, 0, 0, 3, "S1"));
        cubes.add(new Cube(3, 4, 3, 3, "S2"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1] --> size = 27\n" + "[S2] --> size = 27\n");
    }

    @Test
    void negative_coordinates() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(0, 0, 0, 3, "S1"));
        cubes.add(new Cube(-2, -2, -2, 3, "S2"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1, S2] --> size = 53\n");
    }

    @Test
    void six_seprate_cubes() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(-1, 0, 0, 1, "S1"));
        cubes.add(new Cube(1, 0, 0, 1, "S2"));
        cubes.add(new Cube(0, 1, 0, 1, "S3"));
        cubes.add(new Cube(0, -1, 0, 1, "S4"));
        cubes.add(new Cube(0, 0, 1, 1, "S5"));
        cubes.add(new Cube(0, 0, -1, 1, "S6"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1] --> size = 1\n" +
                                             "[S2] --> size = 1\n" +
                                             "[S3] --> size = 1\n" +
                                             "[S4] --> size = 1\n" +
                                             "[S5] --> size = 1\n" +
                                             "[S6] --> size = 1\n"
        );
    }

    @Test
    void multi_fused_cubes_1_wj() {

        List<Cube> cubes = new ArrayList<>();

        cubes.add(new Cube(0, 0, -10, 1, "S1"));
        cubes.add(new Cube(0, 0, -9, 1, "S2"));
        cubes.add(new Cube(0, 0, -8, 1, "S3"));
        cubes.add(new Cube(0, 0, -7, 1, "S4"));
        cubes.add(new Cube(0, 0, -6, 1, "S5"));
        cubes.add(new Cube(3, 0, -10, 1, "S6"));
        cubes.add(new Cube(3, 0, -9, 1, "S7"));
        cubes.add(new Cube(3, 0, -8, 1, "S8"));
        cubes.add(new Cube(3, 0, -7, 1, "S9"));
        cubes.add(new Cube(3, 0, -6, 1, "S10"));
        cubes.add(new Cube(1, 4, -6, 2, "S11"));
        cubes.add(new Cube(0, 0, -5, 2, "S12"));
        cubes.add(new Cube(2, 0, -5, 2, "S13"));
        cubes.add(new Cube(0, 1, -5, 2, "S14"));
        cubes.add(new Cube(2, 1, -5, 2, "S15"));
        cubes.add(new Cube(1, 3, -5, 2, "S16"));
        cubes.add(new Cube(2, 0, -3, 2, "S17"));
        cubes.add(new Cube(1, 2, -3, 2, "S18"));
        cubes.add(new Cube(1, 1, -1, 2, "S19"));
        cubes.add(new Cube(1, 0, 0, 2, "S20"));
        cubes.add(new Cube(1, 0, 2, 2, "S21"));
        cubes.add(new Cube(3, 0, 1, 1, "S22"));
        cubes.add(new Cube(3, 0, 2, 1, "S23"));
        cubes.add(new Cube(3, 0, 3, 1, "S24"));
        cubes.add(new Cube(-9, 2, 6, 1, "S25"));
        cubes.add(new Cube(-10, 0, 5, 2, "S26"));
        cubes.add(new Cube(-9, 0, 5, 2, "S27"));
        cubes.add(new Cube(-9, 0, 6, 2, "S28"));
        cubes.add(new Cube(-11, 0, 4, 1, "S29"));
        cubes.add(new Cube(-10, 0, 4, 1, "S30"));
        cubes.add(new Cube(-9, 0, 4, 1, "S31"));
        cubes.add(new Cube(-8, 0, 4, 1, "S32"));
        cubes.add(new Cube(-7, 0, 4, 1, "S33"));
        cubes.add(new Cube(-7, 0, 5, 1, "S34"));
        cubes.add(new Cube(-7, 0, 6, 1, "S35"));
        cubes.add(new Cube(-7, 0, 7, 1, "S36"));
        cubes.add(new Cube(-7, 0, 8, 1, "S37"));
        cubes.add(new Cube(-8, 0, 8, 1, "S38"));
        cubes.add(new Cube(-11, 0, 5, 1, "S39"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S11, S1, S2, S3, S4, S5, S12, S6, S7, S8, S9, S10, S13, S14, S15, S16, S17, S18, S19, S20, S21, S22, S23, S24] --> size = 89\n" +
                                             "[S25, S26, S27, S28, S29, S30, S31, S32, S33, S34, S35, S36, S37, S38, S39] --> size = 28\n");
    }

    @Test
    void multi_fused_cubes_2() {
        List<Cube> cubes = new ArrayList<>();

        cubes.add(new Cube(2, 2, 2, 1, "S1"));

        cubes.add(new Cube(1, 0, 1, 2, "S2"));

        cubes.add(new Cube(2, 0, 1, 2, "S3"));
        cubes.add(new Cube(2, 0, 2, 2, "S4"));

        cubes.add(new Cube(0, 0, 1, 1, "S5"));
        cubes.add(new Cube(0, 0, 0, 1, "S6"));

        cubes.add(new Cube(1, 0, 0, 1, "S7"));
        cubes.add(new Cube(2, 0, 0, 1, "S8"));
        cubes.add(new Cube(3, 0, 0, 1, "S9"));

        cubes.add(new Cube(4, 0, 1, 1, "S10"));
        cubes.add(new Cube(4, 0, 2, 1, "S11"));
        cubes.add(new Cube(4, 0, 3, 1, "S12"));
        cubes.add(new Cube(4, 0, 4, 1, "S13"));
        cubes.add(new Cube(4, 0, 5, 1, "S14"));
        cubes.add(new Cube(3, 0, 4, 1, "S15"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12, S13, S14, S15] --> size = 28\n");
    }


        @Test
    void multi_fused_cubes_3() {

        List<Cube> cubes = new ArrayList<>();
        cubes.add(new Cube(-4, 0, -4, 2, "S1"));
        cubes.add(new Cube(-2, 0, -4, 2, "S2"));
        cubes.add(new Cube(0, 0, -4, 2, "S3"));
        cubes.add(new Cube(2, 0, -4, 2, "S4"));
        cubes.add(new Cube(2, 0, -2, 2, "S5"));
        cubes.add(new Cube(2, 0, 0, 2, "S6"));
        cubes.add(new Cube(2, 0, 2, 2, "S7"));
        cubes.add(new Cube(-3, 2, -3, 2, "S8"));
        cubes.add(new Cube(-1, 2, -3, 2, "S9"));
        cubes.add(new Cube(1, 2, -3, 2, "S10"));
        cubes.add(new Cube(1, 2, -1, 2, "S11"));
        cubes.add(new Cube(1, 2, 1, 2, "S12"));
        cubes.add(new Cube(-2, 4, -2, 2, "S13"));
        cubes.add(new Cube(0, 4, -2, 2, "S14"));
        cubes.add(new Cube(0, 4, 0, 2, "S15"));
        cubes.add(new Cube(-1, 0, -5, 1, "S16"));
        cubes.add(new Cube(0, 0, -5, 1, "S17"));
        cubes.add(new Cube(-1, 1, -4, 2, "S18"));
        cubes.add(new Cube(-1, 3, -3, 2, "S19"));
        cubes.add(new Cube(4, 0, -1, 1, "S20"));
        cubes.add(new Cube(4, 0, 0, 1, "S21"));
        cubes.add(new Cube(2, 1, -1, 2, "S22"));
        cubes.add(new Cube(1, 3, -1, 2, "S23"));
        cubes.add(new Cube(-1, 6, -1, 2, "S24"));

        String output = fixture.calculateClusters(cubes);
        assertThat(output).isEqualTo("[S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12, S13, S14, S15, S16, S17, S18, S19, S20, S21, S22, S23, S24] --> size = 140\n");
    }


}
