package nl.bitart.cubes2D;


import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class Cubes2DTest {

    private final Space fixture = new Space();


    @Test
    void testCase0() {

        List<Square> squares = new ArrayList<>();

        Square s1 = new Square(0, 0, 2, "S1");
        squares.add(s1);

        Square s2 = new Square(1, -1, 2, "S2");
        squares.add(s2);

        String output = fixture.calculateClusters(squares);
        assertThat(output).isEqualTo("[S1, S2] --> size = 7\n");
    }

    @Test
    void testCase1() {

        List<Square> squares = new ArrayList<>();

        Square s1 = new Square(0, 0, 1, "S1");
        squares.add(s1);

        Square s2 = new Square(1, 0, 1, "S2");
        squares.add(s2);

        String output = fixture.calculateClusters(squares);
        assertThat(output).isEqualTo("[S1, S2] --> size = 2\n");
    }

    @Test
    void testCase2() {

        List<Square> squares = new ArrayList<>();

        Square s1 = new Square(0, 0, 1, "S1");
        squares.add(s1);

        Square s2 = new Square(-1, 0, 1, "S2");
        squares.add(s2);

        String output = fixture.calculateClusters(squares);
        assertThat(output).isEqualTo("[S1, S2] --> size = 2\n");
    }

    @Test
    void testCase3() {

        List<Square> squares = new ArrayList<>();

        Square s1 = new Square(0, 0, 2, "S1");
        squares.add(s1);

        Square s2 = new Square(1, 1, 2, "S2");
        squares.add(s2);

        Square s3 = new Square(2, 2, 2, "S3");
        squares.add(s3);

        Square s4 = new Square(2, 2, 2, "S4");
        squares.add(s4);

        Square s5 = new Square(5, 5, 2, "S5");
        squares.add(s5);

        String output = fixture.calculateClusters(squares);
        assertThat(output).isEqualTo("[S1, S2, S3, S4] --> size = 10\n" +
                                             "[S5] --> size = 4\n");
    }

    @Test
    void testCase4() {

        List<Square> squares = new ArrayList<>();

        Square s1 = new Square(0, 0, 3, "S1");
        squares.add(s1);

        Square s2 = new Square(3, 3, 2, "S2");
        squares.add(s2);

        String output = fixture.calculateClusters(squares);
        assertThat(output).isEqualTo("[S1] --> size = 9\n" + "[S2] --> size = 4\n");
    }

    @Test
    void testCase5() {

        List<Square> squares = new ArrayList<>();

        Square s1 = new Square(0, 0, 3, "S1");
        squares.add(s1);

        Square s2 = new Square(4, 3, 2, "S2");
        squares.add(s2);

        String output = fixture.calculateClusters(squares);
        assertThat(output).isEqualTo("[S1] --> size = 9\n" + "[S2] --> size = 4\n");
    }

    @Test
    void testCase6() {

        List<Square> squares = new ArrayList<>();

        Square s1 = new Square(0, 0, 1, "S1");
        squares.add(s1);

        Square s2 = new Square(2, 0, 1, "S2");
        squares.add(s2);

        Square s3 = new Square(0, 2, 1, "S3");
        squares.add(s3);

        Square s4 = new Square(2, 2, 1, "S4");
        squares.add(s4);

        Square s5 = new Square(1, 1, 1, "S5");
        squares.add(s5);

        String output = fixture.calculateClusters(squares);
        assertThat(output).isEqualTo("[S1] --> size = 1\n" +
                                             "[S2] --> size = 1\n" +
                                             "[S3] --> size = 1\n" +
                                             "[S4] --> size = 1\n" +
                                             "[S5] --> size = 1\n"
        );
    }
}
